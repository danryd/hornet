using System;
using System.Configuration;

namespace Hornet.Core.Tests.ServiceImplementations
{
    public class Appl// : ServerApplication<Server>
    {
        static Server server;
        public static void Main(string[] args)
        {
            server = new Server();
            server.OnStart();
        }
        ~Appl()
        {
            server.OnStop();
        }
        //public override void Configure()
        //{
        //    CreateUsing(() => new Server()).StartBy(s => s.OnStart()).ShutdownBy(s => s.OnStop());
        //}
    }
    public class Server
    {
        private string msg;

        public void OnStart()
        {
            if ((AppDomain.CurrentDomain.GetData("errorMode") as string) == "true")
            {

                AppDomain.CurrentDomain.SetData("errorMode", "executed");
                throw new ApplicationException("error");
            }

            AppDomain.CurrentDomain.SetData("FactData", "started");

            var cfgValue = (string)ConfigurationManager.AppSettings["asetting"];
            AppDomain.CurrentDomain.SetData("config", cfgValue);
            msg = "Starting impl in appdomain: " + AppDomain.CurrentDomain.FriendlyName;
        }

        public void OnStop()
        {
            AppDomain.CurrentDomain.SetData("FactData", "stopped");
            msg = "stopping impl in appdomain: " + AppDomain.CurrentDomain.FriendlyName;
        }
    }

}
