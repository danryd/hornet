using System.Configuration;

namespace Hornet
{
    public class HornetSettings : ConfigurationSection
    {
        private static readonly HornetSettings settings
          = ConfigurationManager.GetSection("Hornet") as HornetSettings;

        public static HornetSettings Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("managementPort"
          , DefaultValue = 8889
          , IsRequired = false)]
        [IntegerValidator(MinValue = 2000
          , MaxValue = 50000)]
        public int ManagementPort
        {
            get { return (int)this["managementPort"]; }
            set { this["managementPort"] = value; }
        }


        [ConfigurationProperty("cachePath"
          , IsRequired = false, DefaultValue = "cache")]
        [StringValidator(InvalidCharacters = "  ~!@#$%^&*()[]{}/;�\"|\\"
          , MinLength = 2
          , MaxLength = 20)]
        public string CachePath
        {
            get { return (string)this["cachePath"]; }
            set { this["cachePath"] = value; }
        }
    }
}
