using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Reflection;
using System.IO;
using System.Threading;
using Hornet.Core;
using Hornet.FileAccess;
using Hornet.Infrastructure;

namespace Hornet
{
    public partial class Service : ServiceBase
    {
        private const string EventSourceName = "Hornet";
        private readonly ServiceEngine engine;
        private static ILogger log = LogFactory.GetLogger("Hornet");

        public Service()
        {
            log.Info("Initializing Service");

            InitializeEventLog();
            InitializeComponent();
            InitializeUnhandeledExceptionHandler();
            var management = SetupManagementInterface();
            engine = new ServiceEngine(new ApplicationScanner(), management);

            log.Info("Initialization complete");
        }

        private void InitializeUnhandeledExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += ExceptionHandler;
        }

        private void InitializeEventLog()
        {
            if (!EventLog.SourceExists(EventSourceName))
            {
                EventLog.CreateEventSource(EventSourceName, "Application");
            }
            AutoLog = true;
        }

        private static ManagementInterface SetupManagementInterface()
        {
            var uri = new Uri(string.Format("http://localhost:{0}", HornetSettings.Settings.ManagementPort));
            var management = new ManagementInterface(uri);
            return management;
        }

        public void Start()
        {
            OnStart(new string[0]);
        }

        protected override void OnStart(string[] args)
        {
            engine.Start(args);
        }

        protected override void OnStop()
        {
            engine.Stop();
        }

        void ExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            var exMessage = BuildExceptionMessage(e);
            var fullMessage = string.Format("Unhandled exception occured{0}{1}", Environment.NewLine, exMessage);
            log.Error(fullMessage);
            EventLog.WriteEntry(EventSourceName, fullMessage, EventLogEntryType.Error);
        }

        static string BuildExceptionMessage(Exception ex)
        {
            string msg = ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine;
            if (ex.InnerException != null)
            {
                msg += Environment.NewLine + BuildExceptionMessage(ex.InnerException);
            }
            return msg;
        }
    }
}
