//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Runtime.Remoting;
//using System.Runtime.Serialization;
//using System.Text;
//using System.Threading;
using Hornet.Infrastructure;
using Hornet.Resources;
using System;
using System.Runtime.Serialization;

namespace Hornet.Core
{
    //    /// <summary>
    //    /// This class is loaded by the server runtime as the entrypoint to the application
    //    /// Is unique per appdomain
    //    /// </summary>
    //    class AppFactory
    //    {
    //        private static readonly ILogger Logger = LogFactory.GetLogger("AppFactory");

    //        private static  currentApplication;
    //        private static Thread executingThread;
    //        /// <summary>
    //        /// Default constructor
    //        /// </summary>
    //        /// <param name="appName">target directory</param>
    //        public AppFactory(string appName)
    //        {

    //            try
    //            {
    //                if (currentApplication != null)
    //                    throw new HostException(Messages.HostExHostAlreadyInitialized);
    //                var apps = GetPossibleApplications(appName);

    //                if (apps.Count > 1)
    //                    throw new HostException(Messages.HostExMultipleDefinitions);
    //                if (apps.Count == 0)
    //                    throw new HostException(Messages.HostExApplicationMissing);
    //                var application = apps[0];

    //                currentApplication = Activator.CreateInstance(application) as IApplication;
    //                if(currentApplication==null)
    //                    throw new HostException(Messages.HostApplicationFailedToInstantiate);
    //                currentApplication.Configure();
    //                currentApplication.Init();

    //                currentApplication.CreateInstance();


    //            }
    //            catch (Exception ex)
    //            {
    //                Logger.Error(Messages.ApplicationCreationError, ex);
    //                throw new CrossBoundaryException("AppFactory encounterd an exception creating application " + ex.Message + Environment.NewLine + ex.StackTrace);
    //            }
    //        }


    //        public static void Start()
    //        {
    //            try
    //            {
    //                Logger.Info("Starting: " + currentApplication.GetType().Name);
    //                executingThread = new Thread(() =>
    //                {
    //                    try { currentApplication.Start(); }
    //                    catch (Exception ex)
    //                    {
    //                        Logger.Error("Error starting application {0}", currentApplication.GetType().Name, ex);
    //                    }
    //                });

    //                executingThread.Start();

    //            }
    //            catch (Exception ex)
    //            {
    //                Logger.Error(Messages.ApplicationCreationError, ex);
    //                throw new CrossBoundaryException("AppFactory encounterd an exception creating application " + ex.Message + Environment.NewLine + ex.StackTrace);

    //            }

    //        }


    //        public static void Shutdown()
    //        {
    //            try
    //            {
    //                if (currentApplication != null)
    //                {
    //                    Logger.Info("Shutting down: " + currentApplication.GetType().Name);
    //                    executingThread.Join();
    //                    currentApplication.Shutdown();
    //                    currentApplication = null;
    //                }
    //                else
    //                {
    //                    Logger.Info("Shutting down disposed app");
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                try
    //                {
    //                    executingThread.Abort();
    //                }
    //                catch (ThreadAbortException tae)
    //                {
    //                    Logger.Warn(Messages.ApplicationShutdownError, tae);

    //                }
    //                Logger.Warn(Messages.ApplicationShutdownError, ex);
    //            }
    //        }

    //        private List<Type> GetPossibleApplications(string appName)
    //        {
    //            Logger.Info("Looking for application for: " + appName);
    //            var foundAssemblies = FindAssemblies(appName);

    //            var apps = new List<Type>();
    //            foreach (var assemblyWithFullPath in foundAssemblies)
    //            {
    //                var assemblyName = Path.GetFileName(assemblyWithFullPath);
    //                apps.AddRange(GetApps(assemblyName));
    //            }
    //            Logger.Info("Found {0} possible application(dir)", apps.Count);
    //            return apps;
    //        }

    //        private static IEnumerable<string> FindAssemblies(string appName)
    //        {
    //            var currentDir = new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName;
    //            var fullDir = Path.Combine(currentDir, appName);
    //            var foundAssemblies = Directory.EnumerateFiles(fullDir).Where(f => f.EndsWith(".dll") || f.EndsWith(".exe")).Select(f => new FileInfo(f).Name);
    //            var allAsms = string.Join(Environment.NewLine, foundAssemblies);
    //            Logger.Info("Found assemblies: " + allAsms);
    //            return foundAssemblies;
    //        }

    //        private static Type[] GetApps(string asmName)
    //        {
    //            var asm = AppDomain.CurrentDomain.Load(Path.GetFileNameWithoutExtension(asmName));

    //            Logger.Info("Loaded assembly: {0}", asmName);
    //            var types = asm.GetTypes();

    //            return types.Where(t => t.BaseType != null 
    //                && (t.BaseType.IsGenericType && t.BaseType.GetGenericTypeDefinition() == typeof(ServerApplication<>))).ToArray();
    //        }
    //    }
    [Serializable]
    public class CrossBoundaryException : Exception
    {
        public CrossBoundaryException()
            : base()
        {

        }
        public CrossBoundaryException(string msg) : base(msg) { }

        public CrossBoundaryException(string msg, Exception innerex) : base(msg, innerex) { }

        public CrossBoundaryException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
