//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Hornet.Core;

//namespace Hornet.Core
//{
//    public abstract class ServerApplication<T> : IApplication where T : class
//    {
//        private T instance;
//        private Action<T> startMethod;
//        private Action<T> shutdownMethod;
//        private Func<T> factoryMethod;

//        public void Start()
//        {
//           startMethod(instance);
//        }
//        public void Shutdown()
//        {
//            shutdownMethod(instance);
//        }

//        public void CreateInstance()
//        {
//            instance = factoryMethod();
//        }
//        internal T Instance()
//        {
//            return instance;
//        }
//        public virtual void Init() {
//            if (startMethod == null)
//                throw new HostException("Start method is not configured");
//            if (shutdownMethod == null)
//                throw new HostException("Shutdown method is not configured");
//            if (factoryMethod == null)
//                throw new HostException("Factory is not configured");
//        }
//        public ServerApplication<T> CreateUsing(Func<T> factory)
//        {
//            factoryMethod = factory;
//            return this;
//        }
//        public ServerApplication<T> StartBy(Action<T> start)
//        {
//            startMethod = start;
//            return this;
//        }
//        public ServerApplication<T> ShutdownBy(Action<T> stop)
//        {
//            shutdownMethod = stop;
//            return this;
//        }

//        public abstract void Configure();

       
//    }

   
//    public interface IApplication
//    {

//        void Start();
//        void Shutdown();
//        void CreateInstance();
//        void Init();
//        void Configure();

//    }

//}
