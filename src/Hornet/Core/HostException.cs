using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hornet.Core
{
    [Serializable]
    class HostException : Exception
    {
        public HostException(string msg):base(msg)
        {
            
        } 
    }
}
