using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Hornet.Infrastructure;
using Hornet.FileAccess;
using System.IO;

namespace Hornet.Core
{
    class ServiceEngine : IDisposable
    {
        private readonly ApplicationScanner scanner;
        private readonly ILogger logger = LogFactory.GetLogger("ServiceEngine");
        private readonly IList<ApplicationHost> applications;
        private readonly ManagementInterface managementInterface;
        public ServiceEngine(ApplicationScanner scanner, ManagementInterface managementInterface)
        {
            this.scanner = scanner;
            this.managementInterface = managementInterface;
            applications = this.scanner.ScanForApplications();
            this.scanner.ApplicationDirectoryCreated += (o, e) =>
            {
                logger.Info("New directory created: {0}", e.Directory);
                if (Directory.Exists(e.Directory))
                {
                    var host = this.scanner.BuildApplication(e.Directory, false);
                    Execute(host);
                    applications.Add(host);
                }
            };
            this.scanner.ApplicationDirectoryDeleted += (o, e) =>
            {
                logger.Info("Directory deleted: {0}", e.Directory);
                if (applications.Any(a => a.Name == e.Directory))
                {
                    var app = applications.First(a => a.Name == e.Directory);
                    applications.Remove(app);
                    app.Shutdown();
                }
            };
            this.scanner.ApplicationDirectoryRenamed += (o, e) =>
            {
                logger.Info("Directory renamed to: {0} from {1} ", e.Directory, e.OldDirectory);
                if (applications.Any(a => a.Name == e.OldDirectory))
                {
                    var app = applications.First(a => a.Name == e.OldDirectory);
                    applications.Remove(app);
                    app.Shutdown();
                }
                if (Directory.Exists(e.Directory))
                {
                    var host = this.scanner.BuildApplication(e.Directory, false);
                    Execute(host);
                    applications.Add(host);
                }

            };

            managementInterface.Start(OnRequest);

            logger.Info("Loaded {0} application(s)", ApplicationCount);
        }

        private string OnRequest(HttpListenerContext ctx)
        {
            var apps = string.Join("<br />", applications.Select(a => a.Name));
            return string.Format("<HTML><BODY>" +
                                 "" +
                                 "ApplicationCount: {0} <br/>" +
                                 "Applications:{1} <br/>" +
                                 "</BODY></HTML>",
                applications.Count, apps);
        }


        public virtual void Start(string[] args)
        {
            foreach (var item in applications)
            {
                try
                {
                    item.LoadFromFolder();
                    item.Start();
                }
                catch (Exception ex)
                {
                    logger.Warn("Failed to start {0}", ex, item);
                }
            }
        }

        private void Execute(ApplicationHost item)
        {
            item.LoadFromFolder();
            logger.Info("Starting: {0}", item.Name);
            item.LoadFromFolder();
            item.Start();
        }

        public virtual void Stop()
        {
            foreach (var item in applications)
            {
                logger.Info("Stopping: {0}", item.GetType().FullName);
                try
                {
                    item.Shutdown();
                    item.Unload();
                    item.Dispose();
                }
                catch (Exception ex)
                {
                    logger.Error("Failed to kill {0}", ex, item.Name);

                }

            }
            applications.Clear();

        }
        public int ApplicationCount { get { return applications.Count(); } }
        public void Dispose()
        {
            managementInterface.Stop();
            Stop();
        }



    }
}
