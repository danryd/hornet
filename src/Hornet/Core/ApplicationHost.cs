using System;
using System.IO;
using System.Runtime.Remoting;
using Hornet.Infrastructure;
using System.Diagnostics;
using Hornet.FileAccess;
using Hornet.Resources;
using System.Threading;

namespace Hornet.Core
{
    //public due to mocking
    [Serializable]
    public class ApplicationHost : IDisposable
    {
        public ApplicationHost(string applicationBase, string applicationDir)
        {
            logger = LogFactory.GetLogger(AppDomain.CurrentDomain.FriendlyName);
            this.applicationBase = applicationBase;
            this.applicationDir = applicationDir;
            Name = applicationDir;
        }

        readonly string applicationBase;
        readonly string applicationDir;

        private string FullPath { get { return Path.Combine(applicationBase, applicationDir); } }
        //[NonSerialized]
        //private static readonly Type AppFactoryType = typeof(AppFactory);
        [NonSerialized]
        private readonly ILogger logger;
        [NonSerialized]
        private DirectoryWatcher watcher;
        public string Name { get; set; }

        public AppDomain Domain { get; set; }
        [NonSerialized]
        private ObjectHandle handle;
        public bool IsLoaded { get { return handle != null; } }

        private bool isUnloaded = false;
        public virtual void LoadFromFolder()
        {

            logger.Info("Creating application {0} with application directory {1} and base directory {2}", applicationDir, applicationDir, applicationBase);

            var setup = BuildAppDomainSetup(applicationDir, applicationDir, applicationBase);
            Domain = AppDomain.CreateDomain(applicationDir, null, setup);
            Domain.DomainUnload += Domain_DomainUnload;
            watcher = new DirectoryWatcher(FullPath);
            watcher.WhenItemDeleted(_ => Shutdown());
            watcher.WhenItemCreated(_ => { if (IsLoaded) Shutdown(); LoadFromFolder(); });
        }

        void Domain_DomainUnload(object sender, EventArgs e)
        {
            if (logger != null)
                logger.Info(Messages.ApplicationHostDomainUnloading);
        }

        void Domain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (Exception)e.ExceptionObject;
            logger.Critical("Appdomain received unhandeled exception - Is terminating: " + e.IsTerminating, ex);
        }


        public AppDomainSetup BuildAppDomainSetup(string name, string applicationBaseDir, string applicationDirectory)
        {
            var setup = new AppDomainSetup();
            var potentialConfigFile = name + ".config";
            if (File.Exists(Path.Combine(applicationDirectory, name + ".config")))
                setup.ConfigurationFile = potentialConfigFile;
            else
                setup.ConfigurationFile = "hornet.config";
            setup.PrivateBinPath = applicationBaseDir;
            setup.ApplicationBase = applicationDirectory;

            setup.ApplicationName = name;
            setup.ShadowCopyFiles = "true";
            setup.CachePath = HornetSettings.Settings.CachePath;
            return setup;
        }

        /// <summary>
        /// Creates a new appdomain, and in the appdomain it start the internal appfactory
        /// </summary>
        public virtual void Start()
        {
            if (Domain == null)
                throw new InvalidOperationException("Domain is null");
            logger.Info("Creating factory for " + Name);

            Domain.UnhandledException += Domain_UnhandledException;
            // handle = Domain.CreateInstance(AppFactoryType.Assembly.FullName, AppFactoryType.FullName, true, 0, null, new object[] { Name }, null, null);
            logger.Info("Factory created, starting...");
            logger.Info("Thread: {0}",Thread.CurrentThread.ManagedThreadId);
            foreach (var executable in Directory.EnumerateFiles(FullPath, "*.exe"))
            {

                Domain.ExecuteAssembly(executable);
            }

            //Domain.DoCallBack(AppFactory.Start);
            logger.Info("Started application");
        }


        public virtual void Shutdown()
        {
            Unload();
            //logger.Info("Shutting down " + Name);
            //if (Domain != null)
            //    Domain.DoCallBack(AppFactory.Shutdown);
        }
        private readonly object _lock = new object();
        public virtual void Unload()
        {
            lock (_lock)
            {
                if (Domain == null || isUnloaded) return;
                logger.Info("Unloading " + Name);
                handle = null;
                watcher.Dispose();
                try
                {
                    AppDomain.Unload(Domain);
                    Domain = null;
                }
                catch (AppDomainUnloadedException adue)
                {
                    logger.Warn("Attempted to unload previously unloaded appdomain", adue);
                }
                isUnloaded = true;
            }

        }

        public void Dispose()
        {
            lock (_lock)
                if (!isDisposed)
                {
                    Unload();
                    GC.SuppressFinalize(this);
                    isDisposed = true;
                }

        }
        private bool isDisposed = false;


    }
}
