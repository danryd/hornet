namespace Hornet.Infrastructure
{
    class LogFactory
    {
        public static ILogger GetLogger(string loggerName)
        {
            return new TraceLogger(loggerName);
        }
    }
}