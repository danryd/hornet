﻿using System;
using System.Net;

namespace Hornet.Infrastructure
{
    /// <summary>
    /// Naive implementation of some metadata for the server
    /// </summary>
    class ManagementInterface : IDisposable
    {
        private readonly Uri uri;
        private Func<HttpListenerContext, string> onRequest;
        private HttpListener listener;
        public ManagementInterface(Uri uri)
        {
            this.uri = uri;
        }

        public virtual void Start(Func<HttpListenerContext, string> onRequest)
        {
            this.onRequest = onRequest;
            listener = new HttpListener();
            listener.Prefixes.Add(uri.AbsoluteUri);
            listener.AuthenticationSchemes = AuthenticationSchemes.IntegratedWindowsAuthentication;
            listener.Start();

            ListenImpl();
        }

// ReSharper disable FunctionRecursiveOnAllPaths
        private async void ListenImpl()
// ReSharper restore FunctionRecursiveOnAllPaths
        {

            var context = await listener.GetContextAsync();

            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;
            // Construct a response. 
            string responseString = onRequest(context);
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;

            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();
            ListenImpl();
        }


        private bool run = true;
        public virtual void Stop()
        {
            if (run)
            {
                run = false;
                listener.Abort();
                listener.Stop();
                listener.Close();
            }
        }

        public virtual void Dispose()
        {
            Stop();

        }
        ~ManagementInterface()
        {
            Stop();
        }
    }
}


