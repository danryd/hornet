using System;
using System.Diagnostics;

namespace Hornet.Infrastructure
{
    internal class TraceLogger : Logger
    {
        private readonly string loggerName;
        private static readonly bool IsConfiguredInAppDomain = false;
        private static readonly TraceSource traceSource;

        static TraceLogger()
        {
            if (!IsConfiguredInAppDomain)
            {
                traceSource = new TraceSource("Hornet");
               
                Trace.UseGlobalLock = true;
                Trace.AutoFlush = true;
                IsConfiguredInAppDomain = true;
            }
        }

        public TraceLogger(string loggerName)
        {
            this.loggerName = loggerName;
        }

        private const string Template = "{0} {1} {2} {3}";
        private const string ExceptionTemplate = "{0} {1}";

        private void LogImpl(string msg, TraceEventType traceEventType, string exceptionMessage, string exceptionStackTrace)
        {
            var exMessage = string.Format(ExceptionTemplate, exceptionMessage, exceptionStackTrace);
            var fullMessage = string.Format(Template, DateTime.Now.ToString("HH:mm:ss"), loggerName, msg, exMessage);
            traceSource.TraceEvent(traceEventType, 0, fullMessage.TrimEnd());
        }

        protected override void Log(string msg, TraceEventType traceEventType, Exception ex, params object[] parameters)
        {
            string exMsg = "";
            string exStackTrace = "";
            if (ex != null)
            {
                GetExceptionInfo(ex, ref exMsg, ref exStackTrace);
            }
           LogImpl(string.Format(msg,parameters), traceEventType, exMsg, exStackTrace);

        }

        protected override void Log(string msg, TraceEventType traceEventType, params object[] parameters)
        {
            Log(msg, traceEventType, null, parameters);
        }

        protected void GetExceptionInfo(Exception ex, ref string exceptionMessage, ref string exceptionStackTrace)
        {
            if (ex.InnerException != null)
            {
                GetExceptionInfo(ex.InnerException, ref exceptionMessage, ref exceptionStackTrace);
            }

            exceptionMessage += ex.Message;
            exceptionStackTrace += ex.StackTrace;
        }
    }
}