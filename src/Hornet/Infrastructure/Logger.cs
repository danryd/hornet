using System;
using System.Diagnostics;

namespace Hornet.Infrastructure
{

    /// <summary>
    /// Logadapter, using it for dev purposes to easily be able to replace it with a proper logengine of choice
    /// </summary>
    public abstract class Logger : ILogger
    {
        public void Info(string msg, params object[] parameters)
        {
            Log(msg, TraceEventType.Information, parameters);
        }

        public void Info(string msg, Exception ex, params object[] parameters)
        {
            Log(msg, TraceEventType.Information, ex, parameters);
        }

        public void Warn(string msg, params object[] parameters)
        {
            Log(msg, TraceEventType.Warning, parameters);
        }

        public void Warn(string msg, Exception ex, params object[] parameters)
        {
            Log(msg, TraceEventType.Warning, ex, parameters);
        }

        public void Error(string msg, params object[] parameters)
        {
            Log(msg, TraceEventType.Error, parameters);
        }

        public void Error(string msg, Exception ex, params object[] parameters)
        {
            Log(msg, TraceEventType.Error, ex, parameters);
        }

        public void Critical(string msg, params object[] parameters)
        {
            Log(msg, TraceEventType.Critical, parameters);
        }

        public void Critical(string msg, Exception ex, params object[] parameters)
        {
            Log(msg, TraceEventType.Critical, ex, parameters);
        }

        protected abstract void Log(string msg, TraceEventType traceEventType, Exception ex, params object[] parameters);
        protected abstract void Log(string msg, TraceEventType traceEventType, params object[] parameters);
    }
}
