using System;

namespace Hornet.Infrastructure
{
    public interface ILogger
    {
        void Info(string msg, params object[] parameters);
        void Info(string msg, Exception ex, params object[] parameters);
        void Warn(string msg, params object[] parameters);
        void Warn(string msg, Exception ex, params object[] parameters);
        void Error(string msg, params object[] parameters);
        void Error(string msg, Exception ex, params object[] parameters);
        void Critical(string msg, params object[] parameters);
        void Critical(string msg, Exception ex, params object[] parameters);
    }
}