﻿using System;
using System.Collections;
using System.IO;
using System.ServiceProcess;
using CommandLineTools;
using Hornet.Infrastructure.WindowsServiceTools;


namespace Hornet.Infrastructure
{
    public class Application
    {
        [Argument("install")]
        public bool Install { get; set; }

        [Argument("uninstall")]
        public bool Uninstall { get; set; }

        [Argument("account", "", "LocalService")]
        public String Account { get; set; }

        public ServiceAccount ServiceAccount { get { return (ServiceAccount)Enum.Parse(typeof(ServiceAccount), Account); } }


        [Argument("startmode", "", "Automatic")]
        public string StartMode { get; set; }

        public ServiceStartMode ServiceStartMode { get { return (ServiceStartMode)Enum.Parse(typeof(ServiceStartMode), StartMode); } }


        [Argument("user")]
        public string Username { get; set; }

        [Argument("password")]
        public string Password { get; set; }

        [Argument("servicename", "Name in service explorer", "Hornet")]
        public string ServiceName { get; set; }

        public void Execute()
        {
            var log = LogFactory.GetLogger("Application");
            var directoryInfo = new FileInfo(GetType().Assembly.Location).Directory;
            if (directoryInfo != null)
                Environment.CurrentDirectory = directoryInfo.FullName;
            if (Install)
            {
                log.Info("Installing");
                var installer = new WindowsServiceInstaller(ServiceName, Username, Password,
                                                            ServiceAccount, ServiceStartMode);
                installer.Install(new Hashtable());
            }
            else if (Uninstall)
            {
                log.Info("Uninstalling");
                var installer = new WindowsServiceInstaller(ServiceName, Username, Password,
                                                          ServiceAccount, ServiceStartMode);
                installer.Uninstall(null);
            }
            else if (Environment.UserInteractive)
            {
                var service = new Service();

                service.Start();
                var exit = false;
                do
                {
                    exit = CheckForExit();
                } while (!exit);
                service.Stop();

            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                    {
                        new Service()
                    };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static bool CheckForExit()
        {
            var key = Console.ReadKey();
            return key.Key == ConsoleKey.C && key.Modifiers == ConsoleModifiers.Control;
        }
    }
}
