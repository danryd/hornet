using CommandLineTools;
using Hornet.Infrastructure;


namespace Hornet
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            var application = Parse.Args<Application>(args);
            application.Execute();
        }
    }
}
