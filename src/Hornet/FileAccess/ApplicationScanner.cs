using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using Hornet.Infrastructure;
using Hornet.Core;

namespace Hornet.FileAccess
{
    public class ApplicationChangeEventArgs : EventArgs
    {
        public string Directory { get; set; }
        public string OldDirectory { get; set; }
    }

    class ApplicationScanner : IDisposable
    {
        private readonly ILogger logger = LogFactory.GetLogger("AssemblyScanner");
        private readonly DirectoryWatcher rootDirectoryWatcher;
        private readonly string rootDirectory = AppDomain.CurrentDomain.BaseDirectory;

        public event EventHandler<ApplicationChangeEventArgs> ApplicationDirectoryCreated;
        public event EventHandler<ApplicationChangeEventArgs> ApplicationDirectoryDeleted;
        public event EventHandler<ApplicationChangeEventArgs> ApplicationDirectoryRenamed;

        public virtual void OnApplicationDirectoryCreated(ApplicationChangeEventArgs e)
        {
            if (ApplicationDirectoryCreated != null) ApplicationDirectoryCreated(this, e);
        }

        public virtual void OnApplicationDirectoryDeleted(ApplicationChangeEventArgs e)
        {
            if (ApplicationDirectoryDeleted != null) ApplicationDirectoryDeleted(this, e);
        }

        public virtual void OnApplicationDirectoryRenamed(ApplicationChangeEventArgs e)
        {
            if (ApplicationDirectoryRenamed != null) ApplicationDirectoryRenamed(this, e);
        }

        public ApplicationScanner()
        {
            rootDirectoryWatcher = new DirectoryWatcher(rootDirectory);
            rootDirectoryWatcher.WhenItemCreated(dir => OnApplicationDirectoryCreated(new ApplicationChangeEventArgs { Directory = dir }));
            rootDirectoryWatcher.WhenItemDeleted(dir => OnApplicationDirectoryDeleted(new ApplicationChangeEventArgs { Directory = dir }));
            rootDirectoryWatcher.WhenItemRenamed((dir, oldDir) => OnApplicationDirectoryRenamed(new ApplicationChangeEventArgs { Directory = dir, OldDirectory = oldDir }));
        }

        public virtual IList<ApplicationHost> ScanForApplications()
        {
            var applicationDirs = Directory.GetDirectories(rootDirectory).Where(d => !d.EndsWith(HornetSettings.Settings.CachePath));
            var apps = new List<ApplicationHost>();
            foreach (var fullSubDir in applicationDirs)
            {
                var app = BuildApplication(new DirectoryInfo(fullSubDir));
                apps.Add(app);
            }
            return apps;
        }

        public ApplicationHost BuildApplication(string dir, bool isFullPath)
        {
            var fullPath = dir;
            if (!isFullPath)
                fullPath = Path.Combine(rootDirectory, dir);
            return BuildApplication(new DirectoryInfo(fullPath));
        }

        public ApplicationHost BuildApplication(DirectoryInfo di)
        {
            logger.Info("Building application host: {0}", di.Name);
            var app = new ApplicationHost(rootDirectory, di.Name);
            return app;
        }

        public void Dispose()
        {
            ApplicationDirectoryCreated = null;
            ApplicationDirectoryDeleted = null;
            ApplicationDirectoryRenamed = null;
            if (rootDirectoryWatcher != null)
                rootDirectoryWatcher.Dispose();
        }
    }



}
