using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Hornet.FileAccess
{
    class DirectoryWatcher :IDisposable
    {
        
        private Action<string> whenItemCreated;
        private Action<string> whenItemDeleted;
        private Action<string,string> whenItemRenamed;
        

        private readonly FileSystemWatcher watcher;

        public DirectoryWatcher(string directoryToWatch)
        {
            // TODO: Complete member initialization
           
            watcher = new FileSystemWatcher();
            watcher.Path = directoryToWatch;
            //watcher.NotifyFilter =  NotifyFilters.DirectoryName|NotifyFilters.;
            watcher.Created += watcher_Created;
            watcher.Deleted += watcher_Deleted;
            watcher.Renamed += watcher_Renamed;
            watcher.EnableRaisingEvents = true;
        }

        private void watcher_Renamed(object sender, RenamedEventArgs e)
        {
            if (whenItemRenamed != null)
                whenItemRenamed(e.Name, e.OldName);
        }

        void watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            if (whenItemDeleted != null)
                whenItemDeleted(e.Name);
        }

        void watcher_Created(object sender, FileSystemEventArgs e)
        {
           
            if (whenItemCreated != null)
                whenItemCreated(e.Name);
        }
        /// <summary>
        /// Directoryname
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public DirectoryWatcher WhenItemCreated(Action<string> callback) {
            whenItemCreated = callback;
            return this;
        }
        /// <summary>
        /// Directoryname
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public DirectoryWatcher WhenItemDeleted(Action<string> callback)
        {
            whenItemDeleted = callback;
            return this;
        }
        /// <summary>
        /// Directory, old directory
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public DirectoryWatcher WhenItemRenamed(Action<string,string> callback)
        {
            whenItemRenamed = callback;
            return this;
        }
        //public DirectoryWatcher WhenFileCreated(Action<string> callback) {
        //    whenFileCreated = callback;
        //    return this;
        //}
        public void Dispose() {
            if (watcher != null)
            {
                watcher.Dispose();
                whenItemCreated = null;
                whenItemDeleted = null;
                whenItemRenamed = null;
            }
        }
    }
}
