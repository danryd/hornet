using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using Hornet.Core;
namespace Hornet.DemoService
{
    public class Program
    {
        private static DemoService service;
        public static void Main(string[] args)
        {
            service = new DemoService();
            service.OnStart();
        }
        ~Program()
        {

            service.OnStop();
        }
    }
    public class DemoService
    {
        private Timer _pulse;
        private const string EventSourceName = "DemoService";
        public DemoService()
        {
            _pulse = new Timer(OnHeartbeat, null, 10, 20000); // every twenty is enough by far just to demo it
            if (!EventLog.SourceExists(EventSourceName))
            {
                EventLog.CreateEventSource(EventSourceName, "Application");
            }
           
        }

        public void OnHeartbeat(object state)
        {
            Write("heartbeat");
        }
        public void OnStart()
        {
            Write("Starting");
        }

        public void OnStop()
        {
            Write("Stopping");
        }


        private void Write(string msg)
        {
            Console.WriteLine("Thread: {0}", Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine(msg);
            EventLog.WriteEntry(EventSourceName, msg.ToString());
        }

    }
}
