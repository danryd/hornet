﻿using CommandLineTools;
using Hornet.Infrastructure;
using Xunit;

namespace Hornet.Core.Tests.Infrastructure
{
    public class ArgsParserTest
    {

        //install/uninstall name account password
        [Fact]
        public void CanParseInstall() {
            var args = new[] { "/install" };

            var parser = Parse.Args<Application>(args);
            Assert.True(parser.Install);
            Assert.False(parser.Uninstall);
        }
        [Fact]
        public void CanParseUninstall()
        {
            var args = new[] { "/uninstall" };

            var parser = Parse.Args<Application>(args);
            Assert.True(parser.Uninstall);
            Assert.False(parser.Install);
        }
    }
}
