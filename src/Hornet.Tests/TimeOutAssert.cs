using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Hornet.Tests
{
    public class TimeOutAssert
    {
        /// <summary>
        /// Run a Fact while input parameter bool returns true or timeout occurs
        /// </summary>
        /// <param name="test">Condition to Fact</param>
        /// <param name="timeoutInMilliseconds"> Milliseconds until Fact times out</param>
        /// <returns> Actual time in ms </returns>
        public static long WaitForCompletion(Func<bool> test, int timeoutInMilliseconds)
        {
            var sw = new Stopwatch();
            sw.Start();
            while (test()) { if (sw.ElapsedMilliseconds > timeoutInMilliseconds) throw new TimeoutException("Test took to long, threading issue"); }
            sw.Stop();
            return sw.ElapsedMilliseconds;
        }
    }
}
