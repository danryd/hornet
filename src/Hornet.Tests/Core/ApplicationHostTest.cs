using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Xunit;

namespace Hornet.Core.Tests.Core
{

    public class ApplicationHostTest : IDisposable
    {

        public ApplicationHostTest()
        {
            deployment = new AppDeploy();

        }
        private readonly AppDeploy deployment;
        private readonly string currentDir = AppDomain.CurrentDomain.BaseDirectory;

        [Fact]
        public void CanLoadFromSelectedFolder()
        {
            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                Assert.Equal(deployment.Target, host.Name);
            }

        }
        [Fact]
        public void CanStartLoadedApplication()
        {
            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                host.Start();
                Thread.Sleep(5);
                var data = host.Domain.GetData("FactData");
                Assert.Equal("started", data);
            }
        }

        [Fact]
        public void CanShutdownLoadedApplication()
        {
            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                host.Start();
                host.Shutdown();
                var data = host.Domain.GetData("FactData");
                Assert.Equal("stopped", data);
            }
        }

        [Fact]
        public void CanGetConfigForApplication()
        {
            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                host.Start();
                Thread.Sleep(350);
                var data = host.Domain.GetData("config");
                Assert.Equal("itsvalue", data);
            }
        }

        [Fact]
        public void CanUnloadApplication()
        {
            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                host.Start();
                host.Unload();
                Assert.False(host.IsLoaded);
            }
        }

        [Fact]
        public void ApplicationExceptionDoesNotPropagateButNotifies()
        {
            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                host.Domain.SetData("errorMode", "true");
                try
                {
                    host.Start();
                    Thread.Sleep(100);
                    var isExecuted = "executed" == host.Domain.GetData("errorMode") as string;
                    Assert.True(isExecuted);
                }
                catch (Exception ex)
                {
                    Assert.False(true, "Should not be here: " + ex.Message);
                }

            }
        }
        [Fact]
        public void ShutsDownOnDelete()
        {

            using (var host = new ApplicationHost(currentDir, deployment.Target))
            {
                host.LoadFromFolder();
                host.Start();
                new DirectoryInfo(deployment.Target).GetFiles()[1].Delete();

                Assert.True(host.IsLoaded);
            }
        }

        public void Dispose()
        {
            deployment.Dispose();
        }
    }
}
