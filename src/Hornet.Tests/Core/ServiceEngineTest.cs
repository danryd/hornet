using System;
using System.Collections.Generic;
using Hornet.Infrastructure;
using Hornet.Tests;
using Xunit;
using Moq;
using Hornet.FileAccess;

namespace Hornet.Core.Tests.Core
{
    public class ServiceEngineTest : IDisposable
    {

        public ServiceEngineTest()
        {
            fake = new FakeLoader(2);
            engine = new ServiceEngine(fake, new NullManagement(null));



        }
        private readonly ServiceEngine engine;
        private readonly FakeLoader fake;
        [Fact]
        public void ServiceEngineStartCallsStartOnAllServices()
        {
            engine.Start(new string[0]);
            TimeOutAssert.WaitForCompletion(() => fake.StartCounter < 2, 80);
            Assert.Equal(2, fake.StartCounter);
        }
        [Fact]
        public void ServiceEngineStopCallsStopOnAllServices()
        {
            engine.Stop();
            Assert.Equal(2, fake.StopCounter);
        }

        [Fact]
        public void CanStartAndStopAllMockServices()
        {
            engine.Start(new string[0]);
            TimeOutAssert.WaitForCompletion(() => fake.StartCounter < 2, 80);
            engine.Stop();
            Assert.Equal(2, fake.StartCounter);
            Assert.Equal(2, fake.StopCounter);
        }

        public void Dispose()
        {
            if (engine != null)
                engine.Dispose();
            if (fake != null)
                fake.Dispose();
        }
    }

    class NullManagement : ManagementInterface
    {
        public NullManagement(Uri uri) : base(uri)
        {
        }

        public override void Start(Func<System.Net.HttpListenerContext, string> onRequest)
        {
            //null
        }
        public override void Stop()
        {
            //null
        }

        public override void Dispose()
        {
            //null
        }
    }
    class FakeLoader : ApplicationScanner
    {
        private int startCounter;
        private int stopCounter;
        public int StartCounter { get { return startCounter; } }
        public int StopCounter { get { return stopCounter; } }
        private readonly List<ApplicationHost> apps;
        private readonly List<Mock<ApplicationHost>> mocks;
        public FakeLoader(int count)
        {
            startCounter = 0;
            startCounter = 0;
            apps = new List<ApplicationHost>();
            mocks = new List<Mock<ApplicationHost>>();
            for (int i = 0; i < count; i++)
            {
                var app = new Mock<ApplicationHost>("", "");

                app.Setup(s => s.Start()).Callback(() => startCounter++);
                app.Setup(s => s.Shutdown()).Callback(() => stopCounter++);

                mocks.Add(app);
                apps.Add(app.Object);
            }
        }

        public override IList<ApplicationHost> ScanForApplications()
        {
            return apps;
        }

    }
}
