﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hornet.Core.Tests
{
    class AppDeploy : IDisposable
    {
        private readonly string[] files = { "hornet.config", "Hornet.Tests.ServiceImplementations.exe" };
        private string target;

        public string Target
        {
            get { return target; }
            private set { target = value; }
        }
        public AppDeploy()
        {
            ClearDirectories();
            target = Guid.NewGuid().ToString();
            Deploy(target);
        }

        private static void ClearDirectories()
        {
            foreach (var item in Directory.GetDirectories(Directory.GetCurrentDirectory()))
            {
                Directory.Delete(item, true);
            }
        }

        protected void Deploy(string to)
        {
            var toDir = new DirectoryInfo(to);
            if (!toDir.Exists)
                toDir.Create();
            foreach (var item in files)
            {
                File.Copy(item, Path.Combine(toDir.FullName, item), true);
            }
        }

        public void Dispose()
        {
            ClearDirectories();
        }
    }
}
