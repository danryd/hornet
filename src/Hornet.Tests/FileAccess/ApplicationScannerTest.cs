using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Hornet.FileAccess;
using Xunit;

namespace Hornet.Core.Tests.FileAccess
{

    public class ApplicationScannerTest
    {

        public ApplicationScannerTest()
        {
            currentDir = new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName;
            if (currentDir != null) appDir = Path.Combine(currentDir, "app");
        }

        private readonly string currentDir;
        private string appDir;
        [Fact]
        public void CanLoadApplications()
        {
            using (var deploy = new AppDeploy())
            {
                using (var loader = new ApplicationScanner())
                {
                    var serviceImplementations = loader.ScanForApplications();
                    Assert.Equal(1, serviceImplementations.Count());
                    foreach (var item in serviceImplementations)
                    {
                        item.Dispose();
                    }
                }
            }
        }

        [Fact]
        public void NewApplicationFolderRaisesCreatedEvent()
        {
            using (var scanner = new ApplicationScanner())
            {
                var dir = Guid.NewGuid().ToString(); ;
                var fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dir);
                var wasRaised = false;
                scanner.ApplicationDirectoryCreated += (o, e) => wasRaised = e.Directory == dir;
                try
                {
                    Directory.CreateDirectory(fullPath);
                    Assert.True(wasRaised);
                    Directory.Delete(fullPath);
                }
                catch (Exception)
                {
                    if (Directory.Exists(fullPath))
                        Directory.Delete(fullPath);

                }
            }

        }
        [Fact]
        public void DeletedFolderRaisesDeletedEvent()
        {
            using (var scanner = new ApplicationScanner())
            {
                var dir = Guid.NewGuid().ToString(); ;
                var fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dir);
                var wasRaised = false;
                scanner.ApplicationDirectoryDeleted += (o, e) => wasRaised = e.Directory == dir;
                try
                {
                    Directory.CreateDirectory(fullPath);
                    Directory.Delete(fullPath);
                    Assert.True(wasRaised);
                }
                catch (Exception)
                {
                    if (Directory.Exists(fullPath))
                        Directory.Delete(fullPath);

                }


            }
        }
    }
}
