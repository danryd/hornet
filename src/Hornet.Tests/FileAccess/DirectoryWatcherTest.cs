using System;
using System.IO;
using System.Threading;
using Hornet.FileAccess;
using Xunit;

namespace Hornet.Core.Tests.FileAccess
{
    public class DirectoryWatcherTest
    {
        private static void CreateAndDeleteDir(string path)
        {
            try
            {
                Directory.CreateDirectory(path);
            }
            finally
            {
                if (Directory.Exists(path)) Directory.Delete(path);
            }
            Thread.Sleep(17); //Sleep a few ms to allow events to occur
        }

        [Fact]
        public void CanWatchRootDirectoryForNewApplications()
        {
            var currentDir = AppDomain.CurrentDomain.BaseDirectory;
            var dw = new DirectoryWatcher(currentDir);

            var created = "";
            var expected = Guid.NewGuid().ToString();
            var fullDir = Path.Combine(currentDir, expected);
            dw.WhenItemCreated(s => created += s);

            CreateAndDeleteDir(fullDir);

            Assert.Equal(expected, created);
        }
        [Fact]
        public void CanWatchDirectoryForChanges()
        {
            var dirName = Guid.NewGuid().ToString();
            using (var dir = new DirectoryWrapper(dirName))
            {
                var currentDir = AppDomain.CurrentDomain.BaseDirectory;
                var fullDir = Path.Combine(currentDir, dirName);
                var dw = new DirectoryWatcher(fullDir);
                var created = "";
                var expected = "file.dll";
                dw.WhenItemCreated(s => created = s);

                var filename = Path.Combine(fullDir, expected);
                var file = File.CreateText(filename);
                file.WriteLine("hi");
                file.Close();

                Thread.Sleep(27);

                Assert.Equal(expected, created);
                dw.Dispose();
                File.Delete(filename);
            }

        }
        [Fact]
        public void RootDirectoryDoesNotReactToSubfolders()
        {
            var dirName = Guid.NewGuid().ToString();
            using (var dir = new DirectoryWrapper(dirName))
            {
                var currentDir = AppDomain.CurrentDomain.BaseDirectory;
                var fullDir = Path.Combine(currentDir, dirName);
                var dw = new DirectoryWatcher(AppDomain.CurrentDomain.BaseDirectory);

                var expected = "file.dll";
                var shouldBeFalse = false;
                dw.WhenItemCreated(s => shouldBeFalse = true);

                var filename = Path.Combine(fullDir, expected);
                var file = File.CreateText(filename);
                file.WriteLine("hi");
                file.Close();

                Thread.Sleep(17);

                Assert.False(shouldBeFalse);
                dw.Dispose();
                File.Delete(filename);
            }
        }

        [Fact]
        public void ActsOnRenamedFolder()
        {
            var dirName = Guid.NewGuid().ToString();
            var dirTo = Guid.NewGuid().ToString();

            var currentDir = AppDomain.CurrentDomain.BaseDirectory;
            var dw = new DirectoryWatcher(currentDir);
            var created = "";
            var dir = Directory.CreateDirectory(dirName);
            dw.WhenItemRenamed((s,old) => created = s);

            dir.MoveTo(dirTo);

            Thread.Sleep(50);
            Assert.Equal(dirTo, created);
            dir.Delete();

        }
        [Fact]
        public void FSW_Exploration()
        {
            var watcher = new FileSystemWatcher();
            watcher.Path = AppDomain.CurrentDomain.BaseDirectory;

            var pathToCreate = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "anewdir");
            var wasCalled = false;
            var name = "";
            var which = "";
            watcher.Created += (o, e) => { wasCalled = true; name += e.Name; which += "created"; };
            watcher.Changed += (o, e) => { wasCalled = true; name += e.Name; which += "changed"; };
            watcher.Deleted += (o, e) => { wasCalled = true; name += e.Name; which += "del"; };
            watcher.Renamed += (o, e) => { wasCalled = true; name += e.Name + " from " + e.OldName; which += "changed"; };
            //watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;


            CreateAndDeleteDir(pathToCreate);
            Thread.Sleep(5);
            Assert.True(wasCalled);
            Console.WriteLine(name + Environment.NewLine + which);
        }

    }

    public class DirectoryWrapper : IDisposable
    {
        private readonly DirectoryInfo directory;
        public DirectoryWrapper(string directoryName)
        {
            var fulldir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, directoryName);
            directory = Directory.CreateDirectory(fulldir);
        }

        public void Dispose()
        {
            directory.Delete(true);
        }

    }
}
