A simple application server for .net

Reference the hornet.exe, inherit from ServerApplication<T> and implement the methods and config. Start the service and thats it!

Example: 
public class DemoApp : ServerApplication<DemoService> {
    public override void Configure()
    {
        CreateUsing(()=> new DemoService())
            .StartBy(x=>x.OnStart())
            .ShutdownBy(x=>x.OnStop());
    }
    }
    public class DemoService 
    {
	...
    }